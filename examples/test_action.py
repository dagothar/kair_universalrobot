#!/usr/bin/python3

import rospy
import actionlib
import control_msgs.msg
from trajectory_msgs.msg import JointTrajectory
from trajectory_msgs.msg import JointTrajectoryPoint


rospy.init_node("test_action")
client = actionlib.SimpleActionClient("/ursim/follow_joint_trajectory", control_msgs.msg.FollowJointTrajectoryAction)
client.wait_for_server()

goal = control_msgs.msg.FollowJointTrajectoryGoal()
goal.trajectory = JointTrajectory()

goal.trajectory.points.append(JointTrajectoryPoint(positions=[0, -1.57, 0, -1.57, 0, 0], time_from_start=rospy.Duration(0.0)))
goal.trajectory.points.append(JointTrajectoryPoint(positions=[0.2, -1.57, 0, -1.57, 0, 0], time_from_start=rospy.Duration(1.0)))
goal.trajectory.points.append(JointTrajectoryPoint(positions=[-0.2, -1.57, 0, -1.57, 0, 0], time_from_start=rospy.Duration(2.0)))

client.send_goal(goal)
client.wait_for_result()
