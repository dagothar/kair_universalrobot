#!/usr/bin/python3

import time
import signal
import sys
import rospy
import socket
import tf

from std_msgs.msg import Bool
from geometry_msgs.msg import Pose
from geometry_msgs.msg import PoseStamped
from geometry_msgs.msg import WrenchStamped
from sensor_msgs.msg import JointState
from kair_universalrobot.msg import *
from kair_universalrobot.srv import *

from ur_interface import URInterface
#from ur_interface import ur_rtde_interface
from ur_followjointtrajectory import URFollowJointTrajectoryActionServer


########################################################################
# Handle Ctrl + C
########################################################################
def sigint_handler(sig, frame):
  sys.exit(0)

signal.signal(signal.SIGINT, sigint_handler)


########################################################################
# Helper functions
########################################################################
def get_local_ip():
  s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
  s.connect(("8.8.8.8", 80))
  ip = s.getsockname()[0]
  s.close()
  return ip
  
  
########################################################################
# Subscribe handlers
########################################################################
def cmd_joint_state_cb(robot, msg):
  robot.servo_to_configuration(msg.position, 1.4, 0.75, 0.008)
  
  
def cmd_joint_speed_cb(robot, msg):
  robot.speed_in_joint_space(msg.velocity, 1.4)


def cmd_tool_speed_cb(robot, msg):
  robot.speed_in_tool_space(msg.velocity, 1.4)
  

########################################################################
# Service handlers
########################################################################
def handle_move_ptp_q(robot, req):
  return robot.move_to_configuration(list(req.target.data), req.a, req.v, req.t, req.r)


def handle_move_lin_q(robot, req):
  return robot.move_to_configuration_linear(list(req.target.data), req.a, req.v, req.t, req.r)
  
  
def handle_move_ptp_p(robot, req):
  return robot.move_to_pose(list(req.target.data), req.a, req.v, req.t, req.r)


def handle_move_lin_p(robot, req):
  return robot.move_to_pose_linear(list(req.target.data), req.a, req.v, req.t, req.r)
  

def handle_servo_q(robot, req):
  return robot.servo_to_configuration(list(req.target.data), req.a, req.v, req.t)


def handle_speed_q(robot, req):
  return robot.speed_in_joint_space(list(req.vel.data), req.a)


def handle_speed_p(robot, req):
  return robot.speed_in_tool_space(list(req.target.data), req.a)
  

def handle_stop(robot, req):
  return robot.stop_robot()


def handle_urscript(robot, req):
  return robot.sendURScript(req.data)


def handle_rg2(robot, req):
  print("RG2(target_width={}, target_force={}, depth_compensation={})\n".format(req.target_width, req.target_force, req.depth_compensation))
  

########################################################################
# Main loop
########################################################################
def main():
  # initialize ROS
  rospy.init_node("ur")
  
  # read parameters
  robot_ip = rospy.get_param('~robot_ip')
  host_port = rospy.get_param('~host_port', 33333)
  command_port = rospy.get_param('~command_port', 30001)
  data_port = rospy.get_param('~data_port', 30003)
  rate = rospy.get_param('~rate', 125)
  ur_script = rospy.get_param('~script', '')
  traj_diag = rospy.get_param('~trajectory_diagnostics', False)
  interface = rospy.get_param('~interface', 0)
  torque_coeffs = rospy.get_param('~torque_coeffs', [0.1094, 0.1100, 0.1097, 0.0820, 0.0822, 0.0824])
  gear_ratios = rospy.get_param('~gear_ratios', [101, 101, 101, 101, 101, 101])
  servoj_lookahead = rospy.get_param('~servoj_lookahead', 0.1)
  servoj_gain = rospy.get_param('~servoj_gain', 300)
  
  # prepare script
  local_ip = ''
  #ur_script = ''
  if interface == 0:
    local_ip = get_local_ip()
    ur_script = ur_script.format(host=local_ip, port=host_port)
  else:
    pass
  
  # connect to UR
  ur = None
  if interface == 0:
    ur = URInterface.URInterface(robot_ip, host_port, command_port, data_port, ur_script)
    ur._torque_coeffs = torque_coeffs
    ur._gear_ratios = gear_ratios
    ur._servoj_lookahead = servoj_lookahead
    ur._servoj_gain = servoj_gain
  else:
    ur = ur_rtde_interface.URRTDEInterface(robot_ip)
  ur.connect()
  
  # initialize subscribers
  cmd_joint_state_sub = rospy.Subscriber('~cmd_joint_q', JointState, lambda msg: cmd_joint_state_cb(ur, msg))
  cmd_joint_speed_sub = rospy.Subscriber('~cmd_joint_speed', JointState, lambda msg: cmd_joint_speed_cb(ur, msg))
  cmd_pose_speed_sub = rospy.Subscriber('~cmd_tool_speed', JointState, lambda msg: cmd_tool_speed_cb(ur, msg))
  
  # initialize publishers
  robot_state_pub = rospy.Publisher('~robot_state', RobotState, queue_size=100)
  joint_state_pub = rospy.Publisher('~joint_states', JointState, queue_size=100)
  tcp_pose_pub = rospy.Publisher('~tcp_pose', PoseStamped, queue_size=100)
  wrench_pub = rospy.Publisher('~wrench', WrenchStamped, queue_size=100)
  is_moving_pub = rospy.Publisher('~is_moving', Bool, queue_size=100)
  currents_pub = rospy.Publisher('~currents', Currents, queue_size=100)
  
  # advertise services
  move_ptp_q_srv = rospy.Service('~move_ptp_q', MovePTP_Q, lambda req: handle_move_ptp_q(ur, req))
  move_lin_q_srv = rospy.Service('~move_lin_q', MoveLin_Q, lambda req: handle_move_lin_q(ur, req))
  move_ptp_t_srv = rospy.Service('~move_ptp_p', MovePTP_P, lambda req: handle_move_ptp_p(ur, req))
  move_lin_t_srv = rospy.Service('~move_lin_p', MoveLin_P, lambda req: handle_move_lin_p(ur, req))
  servo_q_srv = rospy.Service('~servo_q', Servo_Q, lambda req: handle_servo_q(ur, req))
  speed_q_srv = rospy.Service('~speed_q', Speed_Q, lambda req: handle_speed_q(ur, req))
  speed_p_srv = rospy.Service('~speed_p', Speed_P, lambda req: handle_speed_p(ur, req))
  stop_srv = rospy.Service('~stop', Stop, lambda req: handle_stop(ur, req))
  
  # create action server
  follow_action_server = URFollowJointTrajectoryActionServer("~follow_joint_trajectory", ur, rate, traj_diag)
  
  # main loop
  loop_rate = rospy.Rate(rate)
  while not rospy.is_shutdown():
    
    time_now = rospy.Time.now()
    
    # publish robot state
    robot_state_msg = RobotState()
    robot_state_msg.header.stamp = time_now
    robot_state_msg.digital_inputs = ur.digital_inputs
    robot_state_msg.digital_outputs = ur.digital_outputs
    robot_state_msg.is_moving = ur.is_moving()
    robot_state_pub.publish(robot_state_msg)
    
    # publish joint states
    joint_state_msg = JointState()
    joint_state_msg.header.stamp = time_now
    joint_state_msg.name = ['base', 'shoulder', 'elbow', 'wrist_1', 'wrist_2', 'wrist_3']
    joint_state_msg.position = ur.joint_positions[:]
    joint_state_msg.velocity = ur.joint_velocities[:]
    joint_state_msg.effort = ur.joint_torques[:] # TODO: calculate torques based on K_tau
    joint_state_pub.publish(joint_state_msg)
    
    # publish tool position
    tcp_pose_msg = PoseStamped()
    tcp_pose_msg.header.stamp = time_now
    tool_position = ur.tool_position
    tcp_pose_msg.pose.position.x = tool_position[0]
    tcp_pose_msg.pose.position.y = tool_position[1]
    tcp_pose_msg.pose.position.z = tool_position[2]
    # calculate pose quaternion
    q = tf.transformations.quaternion_from_euler(tool_position[3], tool_position[4], tool_position[5])
    tcp_pose_msg.pose.orientation.x = q[0]
    tcp_pose_msg.pose.orientation.y = q[1]
    tcp_pose_msg.pose.orientation.z = q[2]
    tcp_pose_msg.pose.orientation.w = q[3]
    tcp_pose_pub.publish(tcp_pose_msg)
    
    # publish force
    force_msg = WrenchStamped()
    force_msg.header.stamp = time_now
    force = ur.tool_force
    force_msg.wrench.force.x = force[0]
    force_msg.wrench.force.y = force[1]
    force_msg.wrench.force.z = force[2]
    force_msg.wrench.torque.x = force[3]
    force_msg.wrench.torque.y = force[4]
    force_msg.wrench.torque.z = force[5]
    wrench_pub.publish(force_msg)
    
    # publish currents
    currents_msg = Currents()
    currents_msg.header.stamp = time_now
    currents_msg.control_currents = ur.joint_control_currents
    currents_msg.actual_currents = ur.joint_actual_currents
    currents_pub.publish(currents_msg)
    
    is_moving_msg = Bool()
    is_moving_msg.data = ur.is_moving()
    is_moving_pub.publish(is_moving_msg)
    
    loop_rate.sleep()


if __name__ == "__main__":
  main()
