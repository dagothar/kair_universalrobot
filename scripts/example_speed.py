#!/usr/bin/python

import rospy
import math
import time

from sensor_msgs.msg import JointState
from kair_universalrobot.srv import *


def main():
  # initialize ROS
  rospy.init_node("example_speed")
  
  pub = rospy.Publisher("/ur5_1/cmd_joint_speed", JointState, queue_size=100)
  
  loop_rate = rospy.Rate(125)
  t0 = time.time()
  while not rospy.is_shutdown():
    t = time.time() - t0
    v1 = 0.3 * math.sin(1*t)
    v2 = 0.3 * math.sin(1.1*t)
    cmd = JointState()
    cmd.velocity = [v1, v2, 0, 0, 0, 0]
    pub.publish(cmd)
    
    loop_rate.sleep()


if __name__ == "__main__":
  try:
    main()
  except rospy.ROSInterruptException:
    pass
