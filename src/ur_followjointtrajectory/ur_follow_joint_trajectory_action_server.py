import rospy
import actionlib
import control_msgs.msg
from kair_universalrobot.msg import FollowJointTrajectoryActionDiag
import kair_trajectory
import numpy as np


class URFollowJointTrajectoryActionServer:
  
  def __init__(self, name, robot, rate=125, publish_diag=False):
    self._name = name
    self._robot = robot
    self._rate = rate
    self._publish_diag = publish_diag
    pub_diag = rospy.Publisher("~trajectory_diag", FollowJointTrajectoryActionDiag, queue_size=100)
    self._as = actionlib.SimpleActionServer(
      self._name,
      control_msgs.msg.FollowJointTrajectoryAction,
      execute_cb=lambda goal: self.execute_cb(goal, pub_diag),
      auto_start = False
    )
    self._as.start()
    
    
  def __del__(self):
    pass
  
  
  def execute_cb(self, goal, pub_diag):
    
    rospy.loginfo("Received goal")
    
    # Find joint order
    base_index = goal.trajectory.joint_names.index("base")
    shoulder_index = goal.trajectory.joint_names.index("shoulder")
    elbow_index = goal.trajectory.joint_names.index("elbow")
    wrist1_index = goal.trajectory.joint_names.index("wrist_1")
    wrist2_index = goal.trajectory.joint_names.index("wrist_2")
    wrist3_index = goal.trajectory.joint_names.index("wrist_3")
    
    rospy.loginfo("Interpolating trajectory")
    
    # Generate trajectory model
    ts = np.array([p.time_from_start.to_sec() for p in goal.trajectory.points])
    xs = [
      np.array([p.positions[base_index] for p in goal.trajectory.points]),
      np.array([p.positions[shoulder_index] for p in goal.trajectory.points]),
      np.array([p.positions[elbow_index] for p in goal.trajectory.points]),
      np.array([p.positions[wrist1_index] for p in goal.trajectory.points]),
      np.array([p.positions[wrist2_index] for p in goal.trajectory.points]),
      np.array([p.positions[wrist3_index] for p in goal.trajectory.points]),
    ]
    
    
    
    trajectory_control = rospy.get_param("~trajectory_control", "position")
    trajectory_type = rospy.get_param("~trajectory_type", "spline")
    amax = rospy.get_param("~trajectory_amax", 10.0)
    jmax = rospy.get_param("~trajectory_jmax", 100.0)
    trajectories = []
    if trajectory_type == "spline":
      trajectories = [kair_trajectory.Spline3Trajectory.from_points_and_times(ts, xs[i]) for i in range(0, 6)]
    elif trajectory_type == "scurve":
      trajectories = [kair_trajectory.SCurveTrajectory.from_points_and_times(ts, xs[i], amax, jmax) for i in range(0, 6)]
    elif trajectory_type == "linear":
      trajectories = [kair_trajectory.LinearTrajectory.from_points_and_times(ts, xs[i]) for i in range(0, 6)]
    else:
      raise NotImplementedError("Trajectory type {} not implemented!".format(trajectory_type))
      
    rospy.loginfo("Trajectory of length {}s interpolated".format(goal.trajectory.points[-1].time_from_start.to_sec()))
    
    kp = rospy.get_param("~trajectory_p", 1.0)
    kd = rospy.get_param("~trajectory_d", 0.0)
      
    success = True
    t_start = rospy.Time.now()
    t_goal = t_start + goal.trajectory.points[-1].time_from_start
    dur = t_goal - t_start
    t = t_start
    
    position_error = 1.0
    
    # Action loop
    loop_rate = rospy.Rate(self._rate)
    while t <= t_goal: # or position_error > 1e-3:
      
      # Handle pre-empting
      if self._as.is_preempt_requested():
        self._as.set_preempted()
        success = False
        break
      
      # Control robot
      t = rospy.Time.now()
      dtau = t - t_start
      tau = dtau.to_sec()
      if t > t_goal:
        tau = dur.to_sec()
      
      positions_actual = self._robot.joint_positions
      positions_desired = [trajectories[i].x(np.array([tau])) for i in range(0, 6)]
      velocities_actual = self._robot.joint_velocities
      velocities_desired = [trajectories[i].dx(np.array([tau])) for i in range(0, 6)]
      positions_error = [positions_actual[i] - positions_desired[i] for i in range(0, 6)]
      position_error = np.linalg.norm(positions_error)
      velocities_error = [velocities_actual[i] - velocities_desired[i] for i in range(0, 6)]
      effort_actual = self._robot.joint_torques
      
      command = []
      if trajectory_control == "speed":
        speed_cmd = [velocities_desired[i] - kp * positions_error[i] - kd * velocities_error[i] for i in range(0, 6)]
        self._robot.speed_in_joint_space(speed_cmd)
        command = speed_cmd
      elif trajectory_control == "position":
        self._robot.servo_to_configuration(positions_desired)
        command = positions_desired
      else:
        raise NotImplementedError("Trajectory control {} not implemented!".format(trajectory_control))
      
      # Generate feedback
      feedback_msg = control_msgs.msg.FollowJointTrajectoryFeedback()
      feedback_msg.joint_names = ['base', 'shoulder', 'elbow', 'wrist_1', 'wrist_2', 'wrist_3']
      feedback_msg.desired.positions = positions_desired
      feedback_msg.desired.velocities = velocities_desired
      feedback_msg.desired.time_from_start = dtau
      feedback_msg.actual.positions = positions_actual
      feedback_msg.actual.velocities = velocities_actual
      feedback_msg.actual.effort = effort_actual
      feedback_msg.actual.time_from_start = dtau
      feedback_msg.error.positions = positions_error
      feedback_msg.error.velocities = velocities_error
      feedback_msg.error.time_from_start = dtau
      self._as.publish_feedback(feedback_msg)
      
      # publish diagnostic messages
      if self._publish_diag:
        diag_msg = FollowJointTrajectoryActionDiag()
        diag_msg.header.stamp = rospy.Time.now()
        diag_msg.trajectory_time = tau
        diag_msg.position_desired = positions_desired
        diag_msg.position_actual = positions_actual
        diag_msg.velocity_desired = velocities_desired
        diag_msg.velocity_actual = velocities_actual
        diag_msg.command = command
        diag_msg.effort_actual = effort_actual
        diag_msg.tool_position = self._robot.tool_position
        diag_msg.tool_velocity = self._robot.tool_velocity
        pub_diag.publish(diag_msg)
      
      loop_rate.sleep()
    
    self._robot.stop_robot()
    success = True # TODO: check?
  
    # Generate result
    if success:
      self._as.set_succeeded(0) # status?
    


if __name__ == "__main__":
  pass
